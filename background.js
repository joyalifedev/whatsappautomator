var loadingCount = 0;
var lastTestedPhone = "";
var tabIdentifier;
var currentTab;

/* -- INJECTING CONTENT SCRIPT -- */
var injected = false;
chrome.tabs.onUpdated.addListener(function listener(tabId, changeInfo, tab){
  if(!changeInfo.hasOwnProperty("status")) return;
  if(typeof(tab.url) != "undefined" && (tab.url.startsWith('https://web.whatsapp') || tab.url.startsWith('http://web.whatsapp')) && !injected){
    tabIdentifier = tabId;
    currentTab = tab;
    injected = true;

    completedCount = 0;
    nonValidatorRunning = false;

    chrome.scripting.executeScript({
      target: {tabId: tab.id},
      files: ['js/port-management.js', 'content.js']
    });

    SetDifferentUpdateListener();

    console.log("INJECTION LISTENER - SET & REMOVED");
  }
});

/* -- KEEPING TRACK OF PAGE STATUS -- */
var previousLoadingEvent = null;
function SetDifferentUpdateListener(){
  chrome.tabs.onUpdated.addListener(function dlistener(tabId, changeInfo, tab){
    if(typeof(tab.url) != "undefined" && (tab.url.startsWith('https://web.whatsapp') || tab.url.startsWith('http://web.whatsapp'))){

      if(changeInfo.status == "complete"){
        CheckPageState();
      }else if(changeInfo.status == "loading"){
        previousLoadingEvent = changeInfo;        
      }
    }
  });
}

var completedCount = 0;
function CheckPageState(){
  //console.log("checking page state...");

  var status = new Object();
  status['previousLoadingEvent::TYPE'] = typeof(previousLoadingEvent);
  status['previousLoadingEvent'] = previousLoadingEvent;
  
  //console.log(JSON.stringify(previousLoadingEvent));

  /*if((typeof(previousLoadingEvent) != "undefined") && previousLoadingEvent != null){
    status['previousLoadingEvent::URL::TYPE'] = typeof(previousLoadingEvent.url);
    status['previousLoadingEvent::URL'] = previousLoadingEvent.url;
  }*/

  //console.log(status);

  if((typeof(previousLoadingEvent) != "undefined" && previousLoadingEvent) && (typeof(previousLoadingEvent.url) != "undefined" && previousLoadingEvent.url)){
    previousLoadingEvent = null;
    completedCount++;
    console.log("completed count: " + completedCount);
    if(completedCount >= 1){sendPhoneForValidation();}
  }
}

function sendPhoneForValidation(){
  MessageContent({type:"activate-validation-mutators", phone:lastTestedPhone});
}

function Clear(){
  console.log("clearing everything in the service worker...");
  completedCount = 0;
  injected = false;

  MessageAutomator({type : "clearResponse"});
}

function UpdatePhone(data){
  MessageAutomator(data);
}

function tryTestingPhone(phone){
  console.log("validating : " + phone);
  lastTestedPhone = phone;
  chrome.tabs.update(tabIdentifier, {
    active: true,
    url: "https://web.whatsapp.com/send?phone=" + phone 
  }, function(tab){
    completedCount = 0;
    injected = false;
    console.log("update tab...");
  });
}


/* -- COMMUNICATION -- */
var Automator = null;
var Content = null;

chrome.runtime.onConnect.addListener(function(port){
  switch(port.name){
    case 'Automator':
      console.log("Connection granted: " + port.name);
      SetAutomator(port);
      break;
    case 'Content':
      console.log("Connection granted: " + port.name);
      SetContent(port);
      break;
    default:
      console.log("Unknown connection requested: " + port.name);
  }
});

function SetAutomator(port){
  Automator = port;  

  if(Automator){
    console.log("port", Automator);
    //console.log("setting automator listener");
    Automator.onMessage.addListener(function(msg){
      ProcessAutomatorMessages(msg);
    });
  }
}

function SetContent(port){
  Content = port;  

  if(Content){    
    console.log("port", Content);
    Content.onMessage.addListener(function(msg){
      ProcessContentMessages(msg);
    });
  }
}

/* -- CONTENT COMMUNICATION -- */
function MessageContent(msg){
  console.log("-- Sending message to content --");
  console.log(msg, Content);
  console.log("--------------------------------");
  Content.postMessage(msg);
}

function ProcessContentMessages(msg){
  //console.log("MESSAGE FROM CONTENT...", msg);
  if(typeof(msg.type) == "undefined" || !msg.type || Content == null) return;  

  switch(msg.type){
    case "update":
      UpdatePhone(msg);
      break;
    case "debug":
      console.log("Content msg", msg.msg);
      break;
    default:
      console.log("Uncategorized port message", msg);
  }
}

/* -- AUTOMATOR COMMUNICATION -- */
function MessageAutomator(msg){
  Automator.postMessage(msg);
}

function AutomatorReportValidation(phone, state){
  var message = new Object();
  message.phone = phone;
  message.state = state;
  MessageAutomator(JSON.stringify(message));
}

function ProcessAutomatorMessages(msg){
  //"MESSAGE FROM AUTOMATOR...", msg);

  if(typeof(msg.type) == "undefined" || !msg.type || Automator == null) return;  

  switch(msg.type){
    case "validation":
      tryTestingPhone(msg.phone);
      break;
    case "nonvalidatorRunning":
      nonValidatorRunning = true;
      break;    
    case "clear":
      Clear();
      break;    
    case "next":
      saveValidation(msg.data);
      break;    
    default:
  }
}