(function(){

    /* -- SERVICE WORKER CONNECTION -- */
    var portName = "Automator";
    /*var port = ReconnectPort();

    port.onMessage.addListener(function(msg) {
        switch(msg.type){
            case "update":
                Update(msg);
                break;
            default:
        }
    });

    port.onDisconnect.addListener(function(p){
        console.log("AUTOMATOR PORT DISCONNECTED: " + p.name, p);
    });

    function ReconnectPort(){
        return chrome.runtime.connect({name: "Automator"});
    }*/

    function MessageListener(msg){
        switch(msg.type){
            case "update":
                Update(msg);
                break;
            default:
        }
    } 

    PortMane.Connect(portName, MessageListener);
    PortMane.SendMessage(portName, {type: "debug", msg: "automator communication"});

    const KEY_PHONE_INDEX = "debugPhoneIndex";
    const KEY_PHONE_LIST = "debugPhoneList";
    const KEY_PHONE_VALIDATED = "debugPhoneListProcessed"

    const KEY_CSV_FILENAME = "debugCSVFileName"
    const KEY_CSV_HEADERS = "debugCSVHeaders"
    const KEY_CSV_CONTENTS = "debugCSVContents"
    //const KEY_CSV_SAVED = "debugCSVSaved"

    var currentIndex = 0;
    var fileState = false;
    var phoneList = [];
    var validatedPhoneList = {};

    var CSVFileName = "";
    var CSVContent = {};
    var CSVHeaders = "";

    function Init(){
        console.log("automator-online");
        GetCSV();
        loadPhoneIndex();
        loadPhoneList();
        loadValidated();
        Refresh();
    }

    function Refresh(){        
        document.querySelector("#phone-count").innerHTML = currentIndex + "/" + phoneList.length + " Phones";
    }

    function Execute(){
        console.log("Ejecutando lista de teléfono.");
        if(phoneList.length <= 0){
            window.alert("Lista de teléfonos está vacía");
            return;
        }

        if(currentIndex < phoneList.length){
            Validate(currentIndex);
        }else{
            window.alert("List completa");
        }
    }

    function Update(data){        
        console.log("Update", data);
        registerValidated(data.phone, data.state);
    }

    /* -- CSV DATA STORAGE -- */
    function UpdateCSV(){
        Save(KEY_CSV_FILENAME, CSVFileName);
        Save(KEY_CSV_HEADERS, CSVHeaders);
        Save(KEY_CSV_CONTENTS, JSON.stringify(CSVContent));
    }

    function GetCSV(){
        CSVFileName = Load(KEY_CSV_FILENAME);
        CSVHeaders = Load(KEY_CSV_HEADERS);
        CSVContent = JSON.parse(Load(KEY_CSV_CONTENTS));
    }

    function FillCSVField(phone, state){
        var pattern = /"(.*?)"/;
        
        try{
            var lineParts = CSVContent[phone].replace(pattern, "").split(",");
            var value = "no-whatsapp";

            if(lineParts.length == 11){
                value = state? "whatsapp" : "no-whatsapp";
                lineParts.push(value);
            }else{
                console.log("line already has whatsapp field");
            }

            CSVContent[phone] = lineParts.join(",");
        }catch(e){
            console.log(e);
        }
    }

    function GetCSVContents(){
        var contents = "";

        var headers = CSVHeaders.replace(/(\r\n|\n|\r)/gm, "").split(",");
        headers.push("Whatsapp");
        headers = headers.join(",");
        contents += headers;

        contents += "\n";
        
        //console.log("HEADERS", contents);
        
        var keys = Object.keys(CSVContent);        
        keys.forEach(key => {
            var line = CSVContent[key] + "\n";            
            contents += line;
        });
        
        return contents;
    }

    function ClearCSV(){
        localStorage.removeItem(KEY_CSV_FILENAME);
        localStorage.removeItem(KEY_CSV_HEADERS);
        localStorage.removeItem(KEY_CSV_CONTENTS);
        //localStorage.removeItem(KEY_CSV_SAVED);

        CSVFileName = "";
        CSVContent = {};
        CSVHeaders = "";
    }

    /* -- VALIDATION -- */
    function registerValidated(phone, state){        
        console.log("REGISTER VALIDATED FOR : " + phone, state);
        Log(phone + " : " + state);
        FillCSVField(phone, state);
        validatedPhoneList[phone] = state;
        saveValidated();
        UpdateCSV();
        Next();
    }

    /* -- PHONE VALIDATION STORAGE -- */
    function saveValidated(){        
        Save(KEY_PHONE_VALIDATED, JSON.stringify(validatedPhoneList));
    }

    function loadValidated(){
        let vp = JSON.parse(Load(KEY_PHONE_VALIDATED));
        validatedPhoneList = vp? vp : {};
    }

    /* -- PHONE LIST STORAGE -- */
    function savePhoneList(){
        Save(KEY_PHONE_LIST, JSON.stringify(phoneList));
    }

    function loadPhoneList(){
        var plist = JSON.parse(Load(KEY_PHONE_LIST));
        phoneList = plist? plist : [];
    }

    /* -- PHONE PROGRESS STORAGE -- */
    function Next(){
        currentIndex += 1
        savePhoneIndex(currentIndex);
        Refresh();
        Execute();
    }

    function savePhoneIndex(index){
        Save(KEY_PHONE_INDEX, index);
        currentIndex = index;
    }

    function loadPhoneIndex(){
        let index = parseInt(Load(KEY_PHONE_INDEX));
        currentIndex = !index? 0 : index;
    }

    function Validate(index){
        //port.postMessage({type: "validation", phone: phoneList[index]});
        PortMane.SendMessage(portName, {type: "validation", phone: phoneList[index]});        
    }

    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
        //console.log("SW message: " + JSON.stringify(request));
        if(typeof(request.type) != "undefined" && request.type){
            if(request.type == "next"){
                ActivateAutoTask();
            }
        }
    });

    /* -- MENU BUTTONS -- */
    function handleFileSelect(event){
        var element = event.target;        
        var fileList = element.files;
        CheckImport(fileList);
    }

    function CheckImport(fileList){
        var validUpload = fileList.length > 0;
        if(validUpload){
            ClearAll();
            readCSV(fileList[0]);
        }
        
        importButtonUI(validUpload);
    }

    function importButtonUI(state){        
        var classToRemove = state? "fa-file-text" : "fa-check";
        var classToAdd = state? "fa-check" : "fa-file-text";        

        document.querySelector("#import>i").classList.add(classToAdd);
        document.querySelector("#import>i").classList.remove(classToRemove);

        fileState = state;
    }

    function importPhoneList(){
        if(fileState) return;
        document.querySelector('input[type="file"]').click();
    }

    function exportPhoneList(){
        if(Object.keys(validatedPhoneList).length > 0){
            var contents = GetCSVContents();
            SaveFile(contents);
        }else{
            console.log();
        }
    }

    function ClearAll(){
        validatedPhoneList = {};
        phoneList = [];
        currentIndex = 0;        

        localStorage.removeItem(KEY_PHONE_INDEX);
        localStorage.removeItem(KEY_PHONE_LIST);
        localStorage.removeItem(KEY_PHONE_VALIDATED);

        PortMane.SendMessage(portName, {type: "clear"});

        ClearCSV();
        ClearLogs();
        importButtonUI(false);
        Refresh();
    }

    /* -- UTILITIES -- */
    function readCSV(f){
        var reader = new FileReader();
        reader.readAsText(f, "UTF-8");

        CSVFileName = f.name;        
        
        reader.onload = function(evt){
            var rfile = reader.result;
            var lines = rfile.split("\n");

            CSVHeaders = lines[0];

            var columns = "";
            var phone = "";            

            for(var i = 1; i < lines.length; i++){
                columns = lines[i].split(",");
                phone = cleanPhone(columns[3]);

                if(typeof(phone) != "undefined" && phone){
                    phoneList.push(phone);
                    CSVContent[phone] = lines[i].replace("\r", "");
                }
            }

            savePhoneList();
            UpdateCSV();
            Refresh();
        }        
        
        reader.onerror = function(evt){
            window.alert("Ha ocurrido un error con el archivo csv");
        }    
    }

    function cleanPhone(rawPhone){
        var phone = rawPhone;
        if(typeof(rawPhone) != "undefined" && rawPhone){
            phone = phone.split(" ").join("");
            phone = phone.split("-").join("");
            phone = phone.split(".").join("");
            phone = phone.split('"').join("");
            phone = phone.split("(").join("");
            phone = phone.split(")").join("");
            phone = phone.split("|").join("");            
        }

        return phone;
    }

    function Load(key){
        return localStorage.getItem(key);
    }

    function Save(key, data){
        localStorage.setItem(key, data);
    }

    function SaveFile(data){
        //console.log("Saving file..." + data);
        let a = document.createElement("a");
        a.style = "display: none";
        document.body.appendChild(a);

        var file = new Blob([data],
                { type: "text/csv;charset=utf-8" });

        var url = window.URL.createObjectURL(file);

        a.href = url;
        a.download = "validated_" + CSVFileName;
        a.click();

        window.URL.revokeObjectURL(url);
        a.remove();
    }

    function Log(msg){
        var logs = document.querySelector("#logs");

        var p = document.createElement("p");
        p.innerText = msg;

        logs.append(p);
        logs.scrollTop = logs.scrollHeight;
    }

    function ClearLogs(){
        document.querySelector("#logs").innerText = "";
    }

    /* -- EVENTS -- */
    document.querySelector("#autoStart").addEventListener("click", Execute);

    document.querySelector("#clear").addEventListener("click", ClearAll);
    document.querySelector("#export").addEventListener("click", exportPhoneList);
    document.querySelector("#import").addEventListener("click", importPhoneList);

    document.querySelector('input[type="file"]').addEventListener("change", handleFileSelect);

    Init();
})();
