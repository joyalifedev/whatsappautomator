var PortMane = (function(w){
    var PortStatus  = {
        "name" : "",
        "port" : null,
        "status" : "",
        callback : function(e){},        
    }

    var messageTemp = null;
    var portManager = new Object();

    /*
    var port = ReconnectPort();
    port.postMessage({type: "debug", msg: "injected communication"});
    */

    /*
    function AddMessageListener(pName, callback){
        port.onMessage.addListener(function(msg) {
            console.log("message from content");
            switch(msg.type){
                case "activate-validation-mutators":
                    console.log("INIT VALIDATION...");
                    Validate(msg.phone);
                    break;
                case "activate-anti-validation-mutator":
                    //AntiValidate();
                    break;
                case "deactivate-anti-validation-mutator":
                    break;
                default:
                    console.log("MESSAGE TYPE ERROR", msg);
            }
        });
    }
    */

    function InitPortData(port){
        var portStatus = Object.create(PortStatus);
        portStatus.name = port.name;
        portStatus.port = port;
        portStatus.status = "init";

        return portStatus;
    }

    function CheckPortInit(pName){
        return (typeof(portManager[pName]) == "undefined" || portManager[pName] == null);
    }

    function ConnectPort(pName, messageListener){
        console.log(pName + "Port Connected");
        var port = chrome.runtime.connect({name: pName});

        if(CheckPortInit(pName)){
            var pData = InitPortData(port);
            pData.callback = messageListener;

            port.onMessage.addListener(messageListener);
            port.onDisconnect.addListener(OnPortDisconnected);

            //if(typeof(messageQueue[pName]) == "undefined") messageQueue[pName] = new Array();
            
            CheckTempMessage(pName);

            portManager[pName] = pData;
        }

        return port;
    }

    function CheckTempMessage(pName){
        if(messageTemp != null && Object.keys(messageTemp)[0] == pName){
            SendMessage(pName, messageTemp[pName]);
            messageTemp = null;
        }
    }

    function OnPortDisconnected(port){
        console.log("Disconnected: " + port.name, "Trying to reconnect...");
        portManager[port.name].status = "disconnected";
        ConnectPort(port.name, portManager[port.name].callback);
    }

    function SendMessage(portName, message){
        if(portManager[portName].status == "disconnected"){
            messageTemp = {portName, message};
        }else{
            portManager[portName].port.postMessage(message);
        }
    }

    function Disconnect(portName){
        portManager[portName].port.disconnect();
    }

    return{
        "Connect" : ConnectPort,        
        "SendMessage" :  SendMessage,
        "Disconnect" : Disconnect,
        "Data" : portManager
    }
})(window);

console.log("PORT MANAGEMENT INJECTED...");