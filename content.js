(function(w){
console.log("content INJECTED!");

var portName = "Content";
var whatsappLoaded = false;
/*var port = ReconnectPort();
port.postMessage({type: "debug", msg: "injected communication"});

port.onMessage.addListener(function(msg) {
    console.log("message from content");
    switch(msg.type){
        case "activate-validation-mutators":
            console.log("INIT VALIDATION...");
            Validate(msg.phone);
            break;
        case "activate-anti-validation-mutator":
            //AntiValidate();
            break;
        case "deactivate-anti-validation-mutator":
            break;
        default:
            console.log("MESSAGE TYPE ERROR", msg);
    }
});

port.onDisconnect.addListener(function(p){
    console.log("CONTENT PORT DISCONNECTED: " + p.name, port);
});

function ReconnectPort(){
    return chrome.runtime.connect({name: "Content"});
}*/

function MessageListener(msg){
    console.log("message from content");
    switch(msg.type){
        case "activate-validation-mutators":
            
            InitLoadingMutatorObserver().then(() => {
                Validate(msg.phone);
            }).catch((err) => {
                PortMane.SendMessage(portName, {type: "debug", msg: err});
            });
            
            break;
        case "activate-anti-validation-mutator":
            //AntiValidate();
            break;
        case "deactivate-anti-validation-mutator":
            break;
        default:
            console.log("MESSAGE TYPE ERROR", msg);
    }    
}

PortMane.Connect(portName, MessageListener);
PortMane.SendMessage(portName, {type: "debug", msg: "injected communication"});

/*setTimeout(()=>{
    PortMane.Disconnect(portName);
    console.log("DEBUG DISCONNECT: " + portName);
    console.log(PortMane.Data);
}, 5000);*/

/*const container = document.querySelector("#app"); //>_1ADa8 
var nodes = [];

if(typeof(container) != "undefined" && container != null){
    nodes = container.querySelectorAll(":scope > span");
}

const config = { attributes: true, childList: true, subtree: true };

const callback = function(mutationsList, observer) {
    for(const mutation of mutationsList) {
        if (mutation.type === 'childList') {
            ///console.log(mutation.addedNodes, mutation.target);
            //mutation.target
            var overlayContainer = document.querySelector('div._209uk');
            if(overlayContainer != null && overlayContainer.isEqualNode(mutation.target)){
                var addedNodes = overlayContainer.querySelector('span._2J8hu');
                if(addedNodes != null && addedNodes == mutation.addedNodes) console.log("NO WHATSAPP OVERLAY");
            }
        }
    }
};

const observer = new MutationObserver(callback);
console.log(container);

if(container != null){
    observer.observe(container, config);
}*/

/*chrome.runtime.sendMessage({greeting: "hello"}, function(response) {
    console.log("sending back object referece...");
    //console.log("sending message");
});*/

var phoneToValidate;
function Validate(phone){
    phoneToValidate = phone;    
    SimplePhoneValidation();
    //InitMutatorObserver();
}

function ValidateWhatsapp(probablyPhone){
    CancelValidation(false);
    console.log("VALIDATING... " + probablyPhone);
    console.log("phoneElement found!");

    if(typeof(probablyPhone) != "undefined" && probablyPhone != null){
      var phone = cleanPhone(probablyPhone);
      var state = ComparePhones(phone)
      if(!state){
          state = SecondaryCheck(phone);
      }

      PhoneUpdate(phoneToValidate, state);
    }else{
        console.log("No phone on screen");
    }
}

function ComparePhones(p){
    console.log("check cleaning 2");
    var cleanp = p.split("+").join("");
    var cleanptv = phoneToValidate.split("+").join("");
    console.log(cleanp + " vs " + cleanptv);
    return cleanptv.indexOf(cleanp) >= 0;
}

function SecondaryCheck(p){
    var phoneArray = [];
    phoneArray.push(phoneToValidate);
    return bPhoneNumberInArray(p, phoneArray);
}

function bPhoneNumberInArray (targetNum, numArray) {
    var targSanitized   = targetNum.replace (/[^\d]/g, "")
                                   .replace (/^.*(\d{10})$/, "$1");
    //--- Choose a character that is unlikely to ever be in a valid entry.
    var arraySanitized  = numArray.join ('Á').replace (/[^\dÁ]/g, "") + 'Á';
  
    //--- Only matches numbers that END with the target 10 digits.
    return (new RegExp (targSanitized + 'Á') ).test (arraySanitized);
  }

function cleanPhone(rawPhone){
    var phone = rawPhone;
    if(typeof(rawPhone) != "undefined" && rawPhone){
        phone = phone.split(" ").join("");
        phone = phone.split("-").join("");
        phone = phone.split(".").join("");
        phone = phone.split('"').join("");
        phone = phone.split("(").join("");
        phone = phone.split(")").join("");
        phone = phone.split("|").join("");
    }
    return phone;
}

function PhoneUpdate(p, s){    
    var pFix = p.split("+").join("");
    console.log("phone validation completed: " + pFix, s);
    //port.postMessage({type: "update", phone : p, state : s});
    PortMane.SendMessage(portName, {type: "update", phone : pFix, state : s});
}

/*w.addEventListener("message", function(msg){
    console.log("PHONE RECEIVED FOR TESTING...");
    console.log(JSON.stringify(msg));
    ValidateWhatsapp(msg.data.phone);*/
    /*console.log("INJECTED MESSAGE RECIEVED! 2222");
    console.log(msg);*/
//});

var obs = null;
var antiSearcherID = null;
function InitMutatorObserver(){
    antiSearcherID = setInterval(IsPhoneNull, 2000);
    const container = document.querySelector("#app");

    obs = new MutationObserver(function (mutations, observer) {
        for (var i = 0; i < mutations.length; i++) {
            //console.log(mutations[i].target);
            //console.log(mutations[i].addedNodes);
            var target = mutations[i].target;
            if(target.classList.contains("_24-Ff")){
                var phone = target.querySelector("div>div>span");
                ValidateWhatsapp(phone.innerText);
            }
            
            /*if(target.classList.contains("._2Nr6U")){
                console.log("message overlay");
                console.log("message overlay content", mutations[i].addedNodes);
            }*/
        }        
    });

    try{
        obs.observe(container, {childList: true, subtree: true, attributes: false, characterData: false});
    }catch(e){
        console.warn(e);
    }
}

function InitLoadingMutatorObserver(callback){
    return new Promise((resolve, reject) => {
        PortMane.SendMessage(portName, {type: "debug", msg: "injected loading mutator"});

        const html = document.getElementsByTagName("html")[0];

        loadingMutator = new MutationObserver((mutations, obsever) => {
            for(let m of mutations){
                if(m.type === 'attributes' && m.attributeName === 'class'){
                    if(m.target.classList.contains("wf-loading")){                    
                        PortMane.SendMessage(portName, {type: "debug", msg: "WHATSAPP FINISHED LOADING..."});
                        resolve();
                        try {loadingMutator.disconnect();}catch(e){}
                    }
                }
            }
        });

        try{
            loadingMutator.observe(html, {childList:false, subtree:false, attributes:true, characterData:false});
        }catch(e){
            reject("Mutator error");
        }
    });
}

var phrase_es = "El número de teléfono compartido a través de la dirección URL es inválido";
var phrase_en = "Phone number shared via url is invalid.";

function FindPopUpOverlay(){
    //main popup classes
    //html.substring(561+90, 561+90+45);
}

function SimplePhoneValidation(){
    var html = document.getElementsByTagName('body')[0].innerHTML;
    FindPopUpOverlay();
    //console.log("CHECK FOR INVALID PHONE...");
    //var overlay = document.querySelector("._2Nr6U");
    //if(typeof(overlay) != "undefined" && overlay){
        /*var msg = overlay.textContent;
        if(msg && (msg.indexOf("invalid") > 0 || msg.indexOf("inválido") > 0 || msg.indexOf("ne pas valide") > 0)){
            console.log("PHONE IS NOT VALID...");
            CancelValidation(true);
        }*/
        if(html.indexOf(phrase_es) > -1 || html.indexOf(phrase_en) > -1){
            console.log("PHONE IS NOT VALID...");
            CancelValidation(true);
        }else{
            ValidateWhatsapp(phoneToValidate);
        }
    //}
}

function CancelValidation(update){
    console.log("CANCEL VALIDATION..." + update);
    clearInterval(antiSearcherID);
    antiSearcherID = null;
    //EndMutatorObserver();

    if(update)PhoneUpdate(phoneToValidate, false);
}

function EndMutatorObserver(){
    try{
        obs.disconnect();
    }catch(e){
        //console.warn(e);
    }
}

/*var antiValidationRunning = false;
function AntiValidate(){
    if(antiValidationRunning) return;
    antiValidationRunning = true;

    console.log("ANTI VALIDATOR");
    waitForElm('._2Nr6U').then((elm) => {
        console.log('Element is ready');
        console.log(elm.textContent);
    });
}*/

function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }

        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}

//var nobs = null;
/*function InitNonValidationMO(){
    chrome.runtime.sendMessage({type: "nonvalidatorRunning"}, function(response) {
        console.log("virtual handshake! Confirmed running");
    });

    const container = document.querySelector("#app");

    nobs = new MutationObserver(function (mutations, observer) {
        for (var i = 0; i < mutations.length; i++) {            
            var msgOverlay = mutations[i].addedNodes[0];
            if(typeof(msgOverlay) != "undefined" && msgOverlay){
                if(msgOverlay.classList.contains("_209uk")){
                    console.log("No whatsapp - Mutator");
                    EndMutatorObserver();
                    EndNonValiationMO();
                }
            }
        }        
    });

    try{
        nobs.observe(container, {childList: true, subtree: true, attributes: false, characterData: false});
    }catch(e){
        //console.warn(e);
    }
}

function EndNonValiationMO(){
    try{
        nobs.disconnect();
    }catch(e){
        //console.warn(e);
    }
}*/

/*chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    console.log("SW message: " + JSON.stringify(request));
    if(typeof(request.type) != "undefined" && request.type){
        if(request.type == "validation"){
            console.log("PHONE RECEIVED FOR TESTING...");
            phoneToValidate = request.phone;
            InitMutatorObserver();
            //EndNonValiationMO();
        }else if(request.type == "nonvalidation"){
            //console.log("CHECKING FOR PHONE ERROR...");
            //InitNonValidationMO();
        }else if(request.type == "disablenonvalidator"){
            console.log("DISABLE NON VALIDATOR...");
            EndNonValiationMO();
        }
    }    
});*/

//observer.disconnect();
})(window);

